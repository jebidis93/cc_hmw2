from os import listdir
from os.path import isfile, join
import socket

class Words:

    def __init__(self, path='/home/data'):

        self.output = ''
        self.file_details = {'total_words': 0}
        self.path = path
        self.get_file_names()
        self.read_files()
        self.find_maxes()
        self.get_ip()
        self.make_output()

    def get_file_names(self):

        path = self.path
        files = {}
        self.output += 'FILE NAMES\n'
        for fname in listdir(path):
            if fname.endswith('.txt') and isfile(join(path, fname)):
                files[fname] = 0
                #self.output += ' - ' + fname + '\n'
        self.file_details['files'] = files

    def read_files(self):

        path = self.path
        file_details = self.file_details
        files = file_details['files'].keys()
        for fname in files:

            # read file and prune
            f = open(join(path, fname), 'r')
            content = f.read().lower()
            content = content.replace('\n', ' ')
            content = content.replace('\t', ' ')

            words = content.split(' ')
            words = [i for i in words if i != '']
            word_count = len(words)
            
            # add relevant info
            file_details['total_words'] += word_count
            file_details['files'][fname] = word_count
    
    def find_maxes(self):

        files = self.file_details['files']
        max_count = 0
        max_files = []
        for fname, count in files.items():
            if count > max_count:
                max_count = count
                max_files = [fname]
            elif count == max_count:
                max_files.append(fname)

        self.file_details['max_count'] = max_count
        self.file_details['max_files'] = max_files

    def get_ip(self):

        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        self.ip_addr = s.getsockname()[0]

    def make_output(self):
        output = ''
        file_details = self.file_details

        # list file names and total words in file
        output += 'All .txt file(s) and word counts:\n'
        for fname, count in file_details['files'].items():
            output += ' - ' + fname + ' - ' + str(count) + ' words\n'

        # grand total word count
        output += 'Total words across all files:\n'
        output += ' - ' + str(file_details['total_words']) + '\n'

        # list file(s) with maximum words
        output += 'File(s) with the maximum word count of ' + str(file_details['max_count']) + ' words:\n'
        for fname in file_details['max_files']:
            output += ' - ' + fname + '\n'

        # IP address
        output += 'IP address of machine:\n'
        output += ' - ' + self.ip_addr

        # write to file
        f = open('/home/output/result.txt', 'w').write(output)


if __name__ == '__main__':
    Words()
